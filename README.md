# Projeto de Sistema de Informação

 Sem nome definido ainda, se trata de um projeto desenvolvido na disciplina de Sistemas de Informações, ministrada pelo professor Alberto Signoretti, para desenvolver uma aplicação que automatize a criação do Jornal Oficial da Universidade do Estado do Rio Grande do Norte.


## Guia para instalação do Flutter Web e Flutter Desktop

Para conseguir rodar a aplicação em seu dispositivo será necessário seguir os seguintes passos:

* Ter o [Flutter](https://flutter.dev/docs/get-started/install) no seu computador instalado.
* Ter o [Flutter Desktop](https://github.com/google/flutter-desktop-embedding) no mesmo caminho raíz do [Flutter](https://flutter.dev/docs/get-started/install).
* Seguir as instruções para o [Flutter Desktop](https://github.com/google/flutter-desktop-embedding) reconhecer seu sistema operacional, seguindo [Tooling desse tutorial](https://github.com/flutter/flutter/wiki/Desktop-shells#tooling).

* Para o [Flutter Web](https://flutter.dev/web) ser instalado, é necessário realizar o download do [Dart SDK] (https://dart.dev/get-dart) , seguindo os passos do [Tutorial](https://github.com/flutter/flutter/wiki/Desktop-shells#tooling). você trocará o canal do Flutter para o Master, vai atualizar o Flutter e por fim vai usar o comando 'flutter config --enable-web', para habilitar o seu editor visualizar o dart.



# Versionamento 

Autor | Edição | O que mudou?
:---------: | :------: | :------:
Luccas Matheus | 0.0.1 | Primeira Organização do Projeto
Luccas Matheus | 0.0.1.1 | Instruções no Markdown de build da aplicação
Luccas Matheus | 0.1.0  | Adição do Flutter Web no projeto
Luccas Matheus | 0.1.1  | Adição da tela de login, pasta contendo imagem da paleta de cor e alteração                               no pubspec.yaml
Althierfson    | 0.1.2  | Criação do acesso ao banco de dados 
Alberto        | 0.1.3  | Alteração no arquivo main.dart
Althierfson    | 0.1.4  | Configurações de acesso a Real-time Database
Althierfson    | 0.1.5  | Acesso a realTime e funções de teste
Luccas Matheus | 0.1.6  | Adição das telas de usuário e administrador
Althierfson    | 0.1.7  | API: Função adicionar novo doc
Althierfson    | 0.1.8  | API: Função buscar doc real
Alberto        | 0.1.9  | API: Função buscar dos
Althierfson    | 0.1.10 | API: Buscar doc clound
Althierfson    | 0.1.11 | API: Editar Documentos
Alberto        | 0.1.12 | API: Deletar documentos
Althierfson    | 0.1.13 | API: Mudança no metodo de busca real
Althierfson    | 0.1.14 | API: Mudança no metodo de ediçao real
Althierfson    | 0.1.15 | API: Função buscar doc Adm real
Luccas Matheus | 0.1.16 | Tela inicial com login, Tela de login de acesso e Tela de erro de acesso