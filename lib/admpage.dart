import 'package:flutter/material.dart';

class AdminPage extends StatefulWidget {
  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      Container(
        width: MediaQuery.of(context).size.height * 6,
        height: 80,
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color(0xFF171E76),
                Color(0xFF1C1452),
              ],
            ),
            borderRadius: BorderRadius.only(topLeft: Radius.circular(120))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Bem Vindo,",
              textAlign: TextAlign.right,
              style: TextStyle(
                color: Colors.white,
                fontSize: 22,
                fontFamily: 'Roboto',
              ),
            ),
            Text(
              'Administrador',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 22,
                fontFamily: 'Roboto',
              ),
            )
          ],
        ),
      ),
      SizedBox(
        height: 15,
      ),
    ]));
  }
}
