import 'package:flutter/material.dart';

class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      Container(
        width: MediaQuery.of(context).size.height * 6,
        height: 80,
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color(0xFF226CB7),
                Color(0xFF1B3B97),
              ],
            ),
            borderRadius: BorderRadius.only(topLeft: Radius.circular(120))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Bem Vindo,",
              textAlign: TextAlign.right,
              style: TextStyle(
                color: Colors.white,
                fontSize: 22,
                fontFamily: 'Roboto',
              ),
            ),
            Text(
              'Usuário',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 22,
                fontFamily: 'Roboto',
              ),
            )
          ],
        ),
      ),
      SizedBox(
        height: 15,
      ),
    ]));
  }
}
